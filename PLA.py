
# coding: utf-8

# # Introduction
#
# In this homework we will study the **Perceptron Learning Algorithm (PLA)**.
# That is we will first answer to the questions asked in Problem 1.4 from the Learning from Data textbook. Then, we will study the variant of PLA called Adaline, from Problem 1.5.
#

# # Imports

# Import the module *numpy* for the mathematical function and the module *matplotlib* to generate plots.

# In[1]:


import numpy as np
import matplotlib.pyplot as plt
plt.style.use('seaborn-whitegrid')
# get_ipython().run_line_magic('matplotlib', 'inline')


# # Random generation of datasets

# To generate a random datasets as requested in the problems, we have first to make sure that our data are linearly separable, or the PLA will not be able to converge and ends.

# ## Linear function

# Let's define a function that will generate a random linear function $ax + b$. The value $a$ and $b$ are chosen using an uniform distribution in $[-bound, bound]$.

# In[2]:


def generate_lin_fct(bound=10, dim=2):
    return [np.random.uniform(-bound, bound) for _ in range(dim)]


# To compute the side of a point with respect to a linear function $f$, we calculate the cross product $\overrightarrow{uv} \times \overrightarrow{up}$ where $u, v$ are two end points (representing the line $f$) and a point $p$. All points with the cross product that have the same sign, will be on the same side.

# In[3]:


def get_class(f, xp, yp):
    """
    :param f: Linear function that separate the points on the plane
    :param xp: xp coordinate of the point to classify
    :param yp: yp -
    :return: -1 or 1 depending on the side
    """
    a, b = f
    # 2 points on the line u, v
    xu, yu, xv, yv = 0, b, 1, a + b
    # Getting sign using cross product
    uv = [xv - xu, yv - yu]
    up = [xp - xu, yp - yu]
    return -1 if np.cross(uv, up) < 0 else 1


# ## Linearly separable dataset

# Let's define a functions to generate random points on the plane. To avoid any confusion, we will refer to the vectors $x$ in the book as the array *x_train* and the labels of each data as
# the array *y_train*. When we refer to a point on the plane we usually use the convention $(x, y)$.
#
# To generate random datasets we need different values:
#
# - The size of the datasets
# - the bound of the coordinates (to make sure that all the points can be visualized in the same some in plots)
# - the dimension $d$ of the space where each points belongs to $\mathbb{R}^d$. For the moment we only consider 2 dimensional space, and will use another method for higher dimension in exercice (f).
#
# Each generated point will be assigned the label $1$ or $-1$, with respect to the side of function $f$, using the function
# from the section above.
#
# Finally, we standardize all of our generated points point by adding the bias constante coordinate $x_0 = 1$.

# In[53]:


def gen_rand_ds_2d(f, size=20, bound=10):
    dim = 2
    x_train = np.random.uniform(-bound, bound, size=(size, dim))
    y_train = np.array([get_class(f, *point) for point in x_train])
    x_train = np.insert(x_train, 0, 1, axis=1) # insert bias cst
    return x_train, y_train


# # Plots

# We define some functions to show plots. *plot_points()* takes a datasets and scatter the points
# on the plane. Points with the same label (i.e. same side) will have the same color (green or red). *plot_lin()* plots a linear function *ax + b*.

# In[41]:


def plot_points(dataset, marker='o'):
    x_array, y_array = dataset
    x_array = x_array[:, 1:]  # remove bias cst
    cmap = ['red' if i < 0 else 'green' for i in y_array]
    plt.scatter(*zip(*x_array), c=cmap, s=20, marker=marker)


# In[42]:


def plot_lin(a, b, bound=10, label=None):
    x = np.linspace(-bound, bound, 1000) # x coord to calc. ax+b
    plt.plot(x, a*x + b, label=label, linewidth=1)


# In[45]:


def plot_result(dataset, bound, f=None, g=None, other_dset=None):
    """
    Plot (and show) the 2D plane with f and g (linear function),
    and the points of the dataset colored using their respecting labels.
    The axis are limited to [-bound, bound]
    :param f: Linear function (original)
    :param g: Linear function (predicted)
    :param dataset: x_train and y_train
    :param bound: axis limits
    """
    if f is not None: plot_lin(*f, label='f')
    if g is not None: plot_lin(*g, label='g')
    plt.legend()
    plot_points(dataset, marker='o')
    if other_dset is not None: plot_points(other_dset, marker='x')
    plt.ylim(-bound, bound)
    plt.xlim(-bound, bound)
    plt.show()


# # Perceptron Learning Algorithm

# ## Weights related function

# Before implementing the PLA, we will first implement function that will make use of the datasets and the weights.

# ### Prediction
#
# To be able to predict, based on a weights vector $w$, the class of a point (that is $-1$ or $-1$), the *predict(w, x)* function compute the inner product between the column vector $w$ and $x$, that is $w = (w_0, w_1, .., w_n)$ where $w_0$ is the bias. The column vector $x = (x_0, x_1, .., x_n)$ is a sample where $x_0 = 1$ as we consider the bias as a weight. The result computation is the sign of the inner product $w^Tx$.

# In[8]:


def predict(w, x):
    value = np.inner(w, x)
    return 1 if value > 0 else -1


# ### Generate $g$ from $w$

# To be able to compare the learned classification by the PLA, when using elements from dimension two $R^2$, it is necessary to generate the learned linear functions $g$, to compared it with $f$. To do so, we resolve the equation $w_0 + w_1x_1 + w_2x_2 = 0$ for two points $(x_1, 0)$ and $(0, x_2)$. We ommitted $x_0$ as it is the constant 1. We have:
# - $x_1 = \frac{-w_0}{w_1}$ when $x_2 = 0$
# - $x_2 = \frac{-w_0}{w_2}$ when $x_1 = 0$
#
# We have two points on the line of the linear function: $p = (-\frac{w_0}{w_1}, 0)$ and $q = (0, -\frac{w_0}{w_2})$, therefore $g = ax + b$ with $a = -\frac{q_y}{p_x}$ and $b = q_y$
#

# In[9]:


def weights_lin_funct(w):
    """
    Return the linear function ax+b from weights generated by PLA
    :param w: weights
    :return: (a, b) of g function
    """
    p = (-np.divide(w[0], w[1]), 0)
    q = (0, -np.divide(w[0], w[2]))
    return -np.divide(q[1], p[0]), q[1]


# ## Misclassified selection

# We implemented two way to allow the PLA to choose a misclassified sample. *select_deterministric()* return the first misclassified element, where *select_random()* will randomly choose an element from all the misclassified element. An element is considered misclassified when its prediction is different from its label, that is $sign(w^Tx_i) \neq y_i$.

# In[10]:


def select_deterministic(w, x_train, y_train):
    """
    Select the first misclassified element.
    :return: misclassified element or -1 if no such element
    """
    i, j = -1, 0
    while i == -1 and j < len(x_train):
        prediction = predict(w, x_train[j])
        if prediction != y_train[j]:
            i = j
        j += 1
    return (x_train[i], y_train[i]) if i != -1 else None


# In[59]:


def select_random_miscls(w, x_train, y_train):
    """
    Select randomly a misclassified element.
    :return: random misclassified element or -1 if no such element
    """
    misclassified = [i for i in range(len(x_train)) if predict(w, x_train[i]) != y_train[i]]
    i = np.random.choice(misclassified) if len(misclassified) != 0 else -1
    return (x_train[i], y_train[i]) if i != -1 else None


# In[58]:


def select_misclassified(w, dataset, random=False):
    select_funct = select_random_miscls if random else select_deterministic
    return select_funct(w, *dataset)


# ## PLA

# The PLA implementation process as follow:
# - Initialize the weights vector $w = [0, .., 0]$ of size $dimension + 1$, where dimension is the dimension of the space $\mathbb{R}^d$.
# - while a misclassified element exists, take one using the deterministic or random selection, and update $w$ using the selected $(x(t), y(t))$ corresponding to the misclassified points.
#
# The misclassified selection and the update method will be defined depending on the question we try to answer as the PLA
# function take in parameters the selection method and the update function.

# In[13]:


def pla(dataset, update_method, dim=2, random_select=False):
    w = np.zeros(dim + 1)  # n els. + bias
    finished, t = False, 0
    while not finished:
        data = select_misclassified(w, dataset, random=random_select)
        if data is not None:
            x_sample, y_sample = data
            w = update_method(w, x_sample, y_sample)
        else:
            finished = True
        t += 1
    return w, t


# # Problem 1.4: Study the Perceptron Learning Algorithm

# Let's first define the update rule used in the PLA that we have to implement for this problem.
# The standard update rule is the following:
# $w(t+1) = w(t) + y(t)x(t)$

# In[14]:


def standard_update_rule(w, x_miscls, y_miscls):
    return np.add(w, np.multiply(x_miscls, y_miscls))


# ## (a) Generate a linearly separable set of size 20

# We generate a random linear function $f$, and a dataset of 2D space elements of size 20. For each points $(x, y)$ of the dataset they belongs to a fixed interval $x,y \in [-10, 10]$ .

# In[15]:



# In[16]:



# ## Next exercices results
# Since the next exercices (b to e) must plot the same results an just depends on different dataset, the next function plots and prints the results required for those exercices

# In[28]:


def run_pla_on(dataset):
    global f, bound, dim
    w, t = pla(dataset, update_method=standard_update_rule, dim=dim, random_select=False)
    g = weights_lin_funct(w)
    # print(f"hypthesis g (weights): {w}")
    plot_result(dataset=dataset, bound=bound, f=f, g=g)
    print(f"Number of updates: {t}")


# ## (b) PLA on the dataset

# In[18]:



# ## (c) another dataset of size 20

# In[19]:



# ## (d) size = 100



# ## (e) size = 1000

# In[21]:



# ## (f) Dataset on $\mathbb{R}^{10}$

# Our approach in the previous exercice to generate our dataset is highly connected to a 2 dimensionnal space, because the method used to classify the data is to use the cross product of vectors.
#
# For a higher dimension , we will use the definition of the perceptron to generate a weights vector of size $d+1$. We will then generate data of dimension $d$ and the classification will be based on the the sign using the generated weights vector.

# In[22]:


def gen_rand_ds_highdim(size=20, bound=10, dim=2):
    w = np.random.uniform(-bound, bound, size=dim+1)
    x_train = np.random.uniform(-bound, bound, size=(size, dim))
    x_train = np.insert(x_train, 0, 1, axis=1) # insert bias cst
    y_train = np.array([predict(w, x) for x in x_train]) # predict is the sign of the inner product
    return x_train, y_train


# In[23]:


# In[24]:



# ## (g) 100 experiments in $\mathbb{R}^{10}$

# In[25]:




# ## (h) Conclusion

# # Problem 1.5: Adaline

# ## Some util functions

# In[102]:



# Random element selection (from all elements, not only misclassified):

# In[103]:


# ## Adaline algorithm

# The Adaptive linear neuron algorithm works as follow, at each iteration:
#
# - Select a random sample $(x(t), y(t))$
# - Compute the signal $s(t) = w^Tx(t)$
# - if $y(t)\cdot s(t) \le 1$, then update w using the following rule:
#     $w(t+1) = w(t) + \eta \cdot (y(t) \cdot s(t)) \cdot x(t)$
#
# As the next value of the weight vector depends on its initialisation, using a null vector at the beginning will not make the algorithm to be able to update itself, as the next value depends on the signal and the weight vector. In oppose to PLA, we will here initialise $w$ with a vector of value 1. This of course doesn't change the ability of the algorithm to find  a weight vector that works because within the infinite space of all weights vectors, a perceptron algorithm will always manage to find a weight vector for linearly separable data.

# In[104]:


def signal(w, x):
    return np.inner(w, x)

def adaline_update_rule(w, eta, x_sample, y_sample):
    sy = np.multiply(signal(w, x_sample), y_sample)
    etasy = np.multiply(eta, sy)
    return np.add(w, np.multiply(x_sample, etasy))

def select_random(x_train, y_train):
    i = np.random.randint(len(x_train))
    return (x_train[i], y_train[i])

def adaline(dataset, eta, nupdates=1000, dim=2):
    w = np.ones(dim + 1, dtype=np.longdouble)  #dim. + bias
    for t in range(nupdates):
        x_sample, y_sample = select_random(*dataset)
        print(x_sample)
        sy =np.multiply(signal(w, x_sample), y_sample)
        if sy <= 1:
            w = adaline_update_rule(w, eta, x_sample, y_sample)
    return w


# In[107]:

if __name__ == "__main__":
    # np.random.seed(109)
    bound = 1
    f = generate_lin_fct()
    train_dset = gen_rand_ds_2d(f, size=100, bound=bound)
    # print(train_dset)
    test_dset = gen_rand_ds_2d(f, size=10000, bound=bound)
    # plot_result(f=f, dataset=train_dset, bound=bound, other_dset=test_dset)
    eta = 0.001
    w = adaline(train_dset, eta, nupdates=1000, dim=2)
    # w, t = pla(train_dset, update_method=standard_update_rule, dim=2, random_select=False)
    g = weights_lin_funct(w)
    print(f"g = {g}, w = {w}")
    plot_result(f=f, g=g, dataset=train_dset, bound=bound)
