import numpy as np
import matplotlib as plt

import logging

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


class PLA:


    def predict(self, x):
        value = np.inner(self.w, x)
        log.debug(f"Predicted value: {value}")
        return 1 if value > 0 else -1

    def select_misclassified(self):
        log.debug("Selecting misclassified")
        i, j = None, 0
        while i is None and j < self.n:
            prediction = self.predict(self.x_train[j])
            log.debug(f"Testing prediction for #{j} {self.x_train[j]} = {self.y_train[j]};"
                      f"predicted {prediction}")
            if prediction != self.y_train[j]:
                i = j
            j += 1
        if i is not None:
            log.debug(f"Sample #{i} {self.x_train[i]} is misclassified: h(x) = {prediction}")
        else:
            log.debug("No misclassified element found")
        return (self.x_train[i], self.y_train[i]) if i is not None else None

